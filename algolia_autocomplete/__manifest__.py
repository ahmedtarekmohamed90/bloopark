{
    'name': 'Algolia Website Autocomplete',
    'category': 'Website',
    'sequence': 10,
    'author': 'Ahmed Tarek',
    'version': '1.0',
    'description': "Autocomplete functionality for product search in website shop",
    'depends': ['website_sale','website'],
    'data': [
        'views/search.xml',
        'views/header.xml',
        'views/config.xml',
    ],
    'installable': True,
    'application': True,
}
