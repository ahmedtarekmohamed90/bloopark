# -*- coding: utf-8 -*-
from odoo import models, fields, api

class WebSite(models.Model):
    _inherit = 'website'
    algolia_app_id = fields.Char(string="Application ID",)
    algolia_app_key = fields.Char(string="Application Key",)
    algolia_index = fields.Char(string="Index ID",)


class AlgoliaConfiguration(models.TransientModel):
    _inherit = 'website.config.settings'
    _description = 'Configuration attributes for Aloglia'

    algolia_app_id = fields.Char(string="Application ID",related='website_id.algolia_app_id')
    algolia_app_key = fields.Char(string="Application Key",related='website_id.algolia_app_key')
    algolia_index = fields.Char(string="Index ID",related='website_id.algolia_index')


