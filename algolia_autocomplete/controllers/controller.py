import json
from odoo import http, tools, _
from odoo.http import request
from algoliasearch import algoliasearch


class WebsiteSale(http.Controller):
    @http.route(['/shop/get_suggest'], type='http', auth="public", methods=['GET'], website=True)
    def get_suggest_json(self, **kw):
        query = kw.get('query')
        settings = request.env['website'].search([], limit=1)
        client = algoliasearch.Client(settings[0].algolia_app_id, settings[0].algolia_app_key)
        index = client.init_index(settings[0].algolia_index)
        res = index.search(
            query,
            {"attributesToRetrieve": "Name,ID", "hitsPerPage": 20}
        )

        results = []
        for product in res['hits']:
            results.append({'value': product['Name'], 'data': {'id': product['ID'], 'after_selected': product['Name']}})
        return json.dumps({
            'query': 'Unit',
            'suggestions': results
        })
